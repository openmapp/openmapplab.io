module gitlab.com/pages/hugo

go 1.20

require (
	github.com/imfing/hextra v0.8.6 // indirect
	github.com/theNewDynamic/gohugo-theme-ananke v0.0.0-20230203204610-a1a99cf12681 // indirect
)

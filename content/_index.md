---
title: "Welcome to OpenMAPP!"
description: "The OpenMAPP project: Open Meta-Analysis in Particle Physics"
---

OpenMAPP is a [CHIST-ERA](https://www.chistera.eu/) project, funded
from 2024-2026 to develop and promote Open Meta-Analysis in Particle
Physics.

Meta-analysis, or [systematic
review](https://www.cochranelibrary.com/about/about-cochrane-reviews),
is a crucial technique in biomedical science, where the evidence for
or against a proposed treatment is strengthened beyond single studies
by careful statistical combination of many independent studies.

In particle physics, the equivalent is a new subfield of
"reinterpretation" of experimental measurements, applied to novel
physics models not available to, or beyond the computational capacity
of, the original analysis.

OpenMAPP's mission is to lead development of the statistical and
physics machinery needed for rapid and ambitious reinterpretations, to
build the e-infrastructure required for maximum re-use of existing
analyses, and provide experimental teams with the technology and
knowledge to preserve their analysis logic for community re-use.

This work will ensure that the public particle-collider facilities
at CERN and elsewhere have the largest possible physics impact and legacy!

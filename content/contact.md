---
title: Contact
featured_image: ''
omit_header_text: true
description: We'd love to hear from you
type: page
#menu: main
---

To contact the OpenMAPP management board, you may write to {{< cloakemail "openmapp-management@cern.ch" >}}

For each of the member states, the regional coordinators can be contacted using the following addresses:

 - France: Sabine Kraml (PI), LPSC Grenoble, {{< cloakemail "sabine.kraml@lpsc.in2p3.fr" >}}
 - Korea: Sezen Sekmen, Kyungpook National University / CHEP {{< cloakemail "sezen.sekmen@cern.ch" >}}
 - Poland: Andrzej Siodmok, Jagellonian University, {{< cloakemail "a.siodmok@cern.ch" >}}
 - Turkey: Gürkan Kumbaroğlu, Bogazici University, {{< cloakemail "gurkank@bogazici.edu.tr" >}}
 - UK: Andy Buckley, University of Glasgow, {{< cloakemail "andy.buckley@gla.ac.uk" >}}
 
---
title: "About"
description: "The OpenMAPP collaboration"
# menu:
#   main:
#     weight: 1
---

The OpenMAPP project aims to engage physicists from across the world,
in both collider experiment and theory, and potentially beyond, into
synergies with nuclear, atomic and astro-physics.

The core collaboration comprises teams in 5 different countries:
France, Korea, Poland, Turkey and the UK, with lead researchers in
Grenoble (LPSC), Paris (Sorbonne/Curie), Kyungpook, Krakow
(Jagellonian), Istanbul (Bogazici), Glasgow, Durham, and London (UCL).

OpenMAPP members lead multiple projects in particle phenomenology and
software tools, including: ADL, Contur, HEPData, MadAnalysis, Rivet,
SModelS, Spey and TACO, and are deeply connected with Monte Carlo
modelling activities, e.g. the MCnet community, and coordination of
the reinterpretation community via the LPCC LHC Reinterpretation Forum
(RiF).

OpenMAPP funding is provided by the [ANR](https://anr.fr/en/),
[NCN](https://www.ncn.gov.pl/en/o-ncn/zadania-ncn),
[TUBITAK](https://www.tubitak.gov.tr/en), and
[UKRI](https://www.ukri.org/) agencies


{{< callout type="warning" >}}
  TODO: add member pages
{{< /callout >}}
